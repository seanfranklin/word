const path = require('path'),
   webpack = require('webpack'),
       BAP = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
 VueLoader = require('vue-loader/lib/plugin');

module.exports = (env, argv) => {
    const isDev = argv.mode === 'development',
       filePath = isDev ? './' : '';

    return ({
        entry: './src/main.js',
        output: {
            path: path.resolve(__dirname, './www'),
            publicPath: isDev ? '/Word/www/' : '/',
            filename: 'build.js'
        },
        module: {
            rules: [{
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }, {
                test: /\.js$/,
                exclude: /(node_modules|sowpods\.js|owl\.js)/,
                loader: 'babel-loader'
            }, {
                test: /\.vue$/,
                loader: 'vue-loader'
            }, {
                test: /\.(jpg|png)$/,
                loader: `file-loader?name=${filePath}images/[name].[ext]`
            }, {
                test: /\.(otf|ttf|woff|woff2)$/,
                loader: `file-loader?name=${filePath}fonts/[name].[ext]`
            }]
        },
        performance: {
            hints: isDev ? 'warning' : false
        },
        devtool: '#source-map',
        plugins: [
            new VueLoader(),
            new BAP({
                analyzerMode:   'static',
                reportFilename: 'bundle_report.html',
                openAnalyzer:   false
            })
        ]
    })
};
