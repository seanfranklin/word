/* globals app */

/** Only used by 'define' function
import axios from 'axios';
import _filter from 'lodash/filter';
import _partial from 'lodash/partial';
import _trim from 'lodash/trim';
*/

import _get from 'lodash/get';
import _includes from 'lodash/includes';
import _remove from 'lodash/remove';
import _sortBy from 'lodash/sortBy';

export default {
    // Verify string is a real word
    verify(word, dict = window[app.$store.state.settings.dictionary]) {
        return _get(dict, (word.toUpperCase() + '$').split(''), false);
    },

    // Get anagrams from a string of letters
    anagrams(chars, dict = window[app.$store.state.settings.dictionary]) {
        const results = [],
                 bank = chars.toUpperCase().split(''),
             alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
            wildcards = _remove(bank, char => !_includes(alphabet, char)).length;

        (function check(bank, wildcards, trieNode, path) {
            for (let node of Object.keys(trieNode)) {
                if (node === '$') {
                    results.push(path);
                } else {
                    const index = bank.indexOf(node);
                    if (index !== -1) {
                        const _bank = bank.slice();
                        _bank.splice(index, 1);
                        check(_bank, wildcards, trieNode[node], path + node);
                    } else if (wildcards) {
                        check(bank, wildcards-1, trieNode[node], path + node);
                    }
                }
            }
        })(bank, wildcards, dict, '');

        return _sortBy(results);
    },

    // Get longest word from an array of words
    longest(words) {
        return words.length ? words.reduce((a, b) => a.length < b.length ? b : a) : '';
    },

    // Get shortest word from an array of words
    shortest(words) {
        return words.length ? words.reduce((a, b) => a.length > b.length ? b : a) : '';
    },

    // Find words in ordered string of letters
    find(chars, dict = window[app.$store.state.settings.dictionary]) {
        const results = [];
        for (let b = 0; b < chars.length - 2; b++) {
            for (let e = b + 2; e <= chars.length; e++) {
                const word = chars.substring(b, e);
                this.verify(word, dict) && results.push(word);
            }
        }
        return results;
    },

    /** Get word definition from Scrabble website
    define(search, callback) {
        if (!/^[a-zA-Z]+$/.test(search)) {
            return setImmediate(callback, `${search} is not an alphabetic string!`);
        }
        axios
            .post('http://scrabble.hasbro.com/en-us/tools#dictionary')
            .type('application/x-www-form-urlencoded')
            .send('dictWord=' + search.toLowerCase())
            .end((err, res) => {
                if (err) {
                    return setImmediate(callback, err);
                }
                const html = cheerio.load(res.text)('.word-definition').html();

                // OOPS! is returned if not valid Scrabble word
                if (!html || /oops!/i.test(html)) {
                    return setImmediate(callback, `'${search} not found!`);
                }
                // Example HTML: "<h4>MOO</h4>to make the deep, moaning sound of a cow<p>Related Words: <strong>MOOED/MOOING/MOOS</strong></p>"
                const condensed = html.replace(/(\t|\n)+/g, ''),
                           word = condensed.match(/<h4>(.*?)<\/h4>/),
                     definition = condensed.match(/<\/h4>(.*?)<p>/),
                        related = condensed.match(/<strong>(.*?)<\/strong>/);

                setImmediate(callback, null, {
                    word: word && word[1].trim(),
                    definition: definition && definition[1].trim(),
                    related: related && _filter(related[1].split('/').map(_partial(_trim, _)))
                });
            });
    } */

};
