export default function loadScript(url, callback) {
    const head = document.querySelector('head'),
        script = document.createElement('script');
    script.async = true;
    script.src = url;
    callback && (script.onload = callback);
    head.appendChild(script);
}
