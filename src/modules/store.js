import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import shortid from 'shortid';
import _get from 'lodash/get';
import { getLetterSet } from './letters';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['id', 'settings', 'stats', 'unlocked']
    })],

    state: {
        difficulties: {
            easy:   { background: 'bubbles',  timer: 120, computer: 'Ronny Rustypins' },
            normal: { background: 'wiggles',  timer: 60,  computer: 'Minnie Maxadder' },
            hard:   { background: 'diamonds', timer: 30,  computer: 'Larry Lightfibers' }
        },
        settings: {
            name:       '',
            country:    'Unknown',
            difficulty: 'easy',
            tileset:    'plastic',
            dictionary: 'SOWPODS'
        },
        stats: {
            best: {
                wordLength: { word: '', value: 0 },
                wordScore:  { word: '', value: 0 },
                roundScore: { word: '', value: 0 },
                gameScore:  70
            },
            average: {
                wordLength: { count: 0, total: 0, value: 0 },
                wordScore:  { count: 0, total: 0, value: 0 },
                roundScore: { count: 0, total: 0, value: 0 },
                gameScore:  0
            }
        },
        unlocked: {
            difficulty: ['easy', 'normal', 'hard'],
            tileset:    ['random', 'plastic', 'maple', 'mahogany','metal', 'marble', 'jade', 'pearl', 'ruby']
        },
        id:         shortid.generate(),
        games:      [],
        letterSets: [],
        timeLeft:   0,
        timeOffset: 0,
        view:       'menu'
    },

    mutations: {
        updateSettings(state, settings) {
            [...Object.keys(settings)].forEach(key => {
                state.settings[key] = settings[key];
            });
        },

        addGame(state) {
            const name = (state.view === 'computer') ? state.difficulties[state.settings.difficulty].computer : '';
            state.games.push({
                opponent: { type: state.view, name, score: 0 },
                player:   { score: 0 },
                rounds:   []
            });
        },

        addRound(state) {
            state.games.length && state.games[state.games.length - 1].rounds.push({
                letterSet: state.letterSets.length ? state.letterSets.shift() : getLetterSet(),
                opponent:  { selected: '', score: { word: -1, length: 0, time: 0, total: -1 } },
                player:    { selected: '', score: { word: -1, length: 0, time: 0, total: -1 } }
            });
        },

        removeLastGame(state) {
            if (state.games.length) {
                const game = state.games[state.games.length - 1];
                game.rounds.length || state.games.pop();
            }
        },

        removeLastRound(state) {
            state.games.length && state.games[state.games.length - 1].rounds.pop();
        },

        updateRound(state, obj) {
            const rounds = state.games.length ? state.games[state.games.length - 1].rounds : [],
             roundPlayer = rounds.length ? rounds[rounds.length - 1][obj.player] : null;
            delete obj.player;
            roundPlayer && [...Object.keys(obj)].forEach(key => {
                roundPlayer[key] = obj[key];
            });
        },

        updateStats(state, obj) {
            const category = state.stats[obj.category];
            delete obj.category;
            category && [...Object.keys(obj)].forEach(key => {
                category[key] = obj[key];
            });
        },

        updateGameScore(state, obj) {
            state.games[state.games.length - 1][obj.player].score += obj.score;
        },

        updateOpponent(state, obj) {
            const opponent = state.games[state.games.length - 1].opponent;
            opponent && [...Object.keys(obj)].forEach(key => {
                opponent[key] = obj[key];
            });
        },

        resetTimer(state) {
            state.timeLeft = state.difficulties[state.settings.difficulty].timer;
            state.timeOffset = 0;
        },

        clearLetterSets(state) { state.letterSets = []; },
        updateLetterSets(state, letterSets) { state.letterSets = state.letterSets.concat(letterSets); },
        updateTimeLeft(state, timeLeft) { state.timeLeft = timeLeft; },
        decrementTimeOffset(state) { state.timeOffset -= 2; },
        setView(state, view) { state.view = view; }
    },

    getters: {
        currentGame: state => {
            const games = state.games;
            return games.length ? games[games.length - 1] : {};
        },

        currentRound: (state, getters) => {
            const rounds = getters.currentGame.rounds || [];
            return rounds.length ? rounds[rounds.length - 1] : {};
        },

        letterSet: (state, getters) => {
            return getters.currentRound.letterSet || getLetterSet(' WORD! ');
        },
        roundCount: (state, getters) => {
            return _get(getters.currentGame, 'rounds.length', 0);
        },
        computer: state => {
            return state.difficulties[state.settings.difficulty].computer;
        },
        baseTimer: state => {
            return state.difficulties[state.settings.difficulty].timer;
        },
        timer: (state, getters) => {
            return getters.baseTimer + state.timeOffset;
        },
        timeLeftPc: (state, getters) => {
            return state.timeLeft / getters.timer * 100;
        },

        opponent: (state, getters) => {
            return {
                name:       _get(getters.currentGame,  'opponent.name', ''),
                country:    _get(getters.currentGame,  'opponent.country', ''),
                selected:   _get(getters.currentRound, 'opponent.selected', ''),
                gameScore:  _get(getters.currentGame,  'opponent.score', 0),
                roundScore: _get(getters.currentRound, 'opponent.score', {})
            };
        },

        player: (state, getters) => {
            return {
                selected:   _get(getters.currentRound, 'player.selected', ''),
                gameScore:  _get(getters.currentGame,  'player.score', 0),
                roundScore: _get(getters.currentRound, 'player.score', {})
            };
        },

        scoreSummary: (state, getters) => {
            const plrWords = [],
                  oppWords = [],
               //numberOfRounds = getters.roundCount,
                   totalScore = [getters.player.gameScore, getters.opponent.gameScore],
                   validWords = [0, 0],
               bestRoundScore = [-1, -1];

            getters.currentGame.rounds && getters.currentGame.rounds.forEach(round => {
                const player = round.player,
                    opponent = round.opponent;
                if (player.score.word > 0) {
                    validWords[0]++;
                    plrWords.push(player.selected);
                }
                if (opponent.score.word > 0) {
                    validWords[1]++;
                    oppWords.push(opponent.selected);
                }
                if (player.score.total > bestRoundScore[0]) {
                    bestRoundScore[0] = player.score.total;
                }
                if (opponent.score.total > bestRoundScore[1]) {
                    bestRoundScore[1] = opponent.score.total;
                }
            });
            const plrTotalWordLen = plrWords.reduce((total, word) => total + word.length, 0),
                  oppTotalWordLen = oppWords.reduce((total, word) => total + word.length, 0),

                averageWordLength = [
                    parseInt((plrTotalWordLen / (plrWords.length || 1)).toFixed(0)),
                    parseInt((oppTotalWordLen / (oppWords.length || 1)).toFixed(0))
                ];
            return { validWords, averageWordLength, bestRoundScore, totalScore };
        }
    }
});
