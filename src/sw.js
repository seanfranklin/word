'use strict';

const cacheName = 'word_cache_v1',

    // Core assets to download and cache on first load
    assets = [
        '/',
        'index.html',
        'build.js',
        'sowpods.js',
        'primus.js',
        'fonts/Raleway-Bold.woff2',
        'fonts/Raleway-Regular.woff2',
        'images/felt.jpg',
        'images/panel.jpg',
        'images/jade.jpg',
        'images/mahogany.jpg',
        'images/maple.jpg',
        'images/marble.jpg',
        'images/pearl.jpg',
        'images/ruby.jpg',
        'images/expressions.png',
        'images/flags.png',
        'sounds/click.webm',
        'sounds/plastic.webm',
        'sounds/wood.webm',
        'sounds/metal.webm',
        'sounds/stone.webm',
        'sounds/whoosh.webm',
    ],

    // Non-essential assets that should be cached if downloaded
    additional = [
        'owl.js'
    ];


/**
 * Open versioned cache from the promise-based caches API.
 * Request assets for offline use and add them to cache.
 */

self.addEventListener('install', ev => {
    //console.info('Event: install service worker');
    ev.waitUntil(caches.open(cacheName).then(cache => cache.addAll(assets)));
});


/**
 * Activation fails unless promise is resolved to an array of available cache keys.
 * Filter by keys that don't match cacheName and delete old caches.
 */

self.addEventListener('activate', ev => {
    //console.info('Event: activate service worker');
    ev.waitUntil(caches.keys().then(keys => {
        return Promise.all(keys.filter(key => key !== cacheName)
            .map(key => caches.delete(key)));
    }).then(self.clients.claim()));
});


/**
 * Intercept request for asset and check for match in cache.
 * Update cache with fresh version of asset from server.
 * Return cached response or fall back to waiting for remote server.
 */

self.addEventListener('fetch', ev => {
    const req = ev.request,
          url = req.url,
         file = url.substring(url.lastIndexOf('/') + 1, url.length).trim();
    ev.respondWith(caches.match(req).then(cached => {
        cached && //console.info(`${file || url} found in cache`);
        return cached || fetch(req).then(res => {
            //console.info(`${file || url} fetched from server`);
            if (~additional.indexOf(file)) {
                caches.open(cacheName).then(cache => {
                    cache.put(req, res.clone());
                    //console.info(`${file || url} added to cache`);
                });
            }
            return res;
        }).catch(() => {
            console.warn(`Failed to fetch ${file || url} from server`);
        });
    }));
});
