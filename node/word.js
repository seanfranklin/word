/* globals __dirname */
'use strict';

const http = require('http'),
    Primus = require('primus'),

   letters = 'EARIOTNSLCUDPMHGBFYWKVXZJQ ',
   weights = '975555554333333222221111110',
    points = '111111111111111222223333330',

   players = {},
    sparks = {},
      pool = [],

    primus = Primus.createServer({
        iknowhttpsisbetter: true,
        pathname:    '/ws',
        transformer: 'websockets',
        hostname:    'localhost',
        port:        30503,
        timeout:     20000,
        maxLength:   32768
    });


//console.log('Created Primus websocket server on port 30503');
primus.plugin('fortress maximus', require('fortress-maximus'));
//console.log('Loaded fortress maximus validation plugin');

// Client library needs to be re-saved after server config change.
// This is not performed automatically when the script runs because
// that would require node to have write access to the www folder.
//primus.save(__dirname.replace('node', 'www') + '/primus.js');
//console.log('Saved primus.js client library to \'www\'');


function getTotalWeight() {
    return weights.split('').reduce((t, w) => t + parseInt(w), 0);
}

function getRndInt(min = 0, max = 100) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getRndLetter() {
    const rnd = Math.random() * getTotalWeight();
    for (let i = 0, sum = 0; i < letters.length; i++) {
        sum += parseInt(weights.charAt(i)) ;
        if (rnd <= sum) return letters.charAt(i);
    }
}

function getPoints(letter) {
    const p = parseInt(points.charAt(letters.indexOf(letter)));
    return isNaN(p) ? 0 : p;
}

function getLetterSet(numOrStr = 7) {
    let cons = 0,
      vowels = 0,
         set = [],
         num = typeof numOrStr === 'number' ? numOrStr : typeof numOrStr === 'string' ? numOrStr.length : 7,
         str = typeof numOrStr === 'string' ? numOrStr : '';

    for (let i = 0; set.length < num; i++) {
        const letter = str.charAt(i) || getRndLetter(),
             isVowel = !!~'AEIOU'.indexOf(letter),
               bgPos = `${getRndInt(1, 90)}% ${getRndInt(1, 90)}%`;

        if (str || (!isVowel && cons++ < 5) || (isVowel && vowels++ < 5)) {
            const points = getPoints(letter);
            set.push({ letter, points, bgPos, selected: false });
        }
    }
    return set;
}

function getLetterSets(num = 100) {
    const sets = [];
    while (sets.length < num) {
        sets.push(getLetterSet());
    }
    return sets;
}

function removeFromPool(id) {
    const index = pool.indexOf(id);
    index >= 0 && pool.splice(index, 1);
}


// nextRound, playWord and updateName effectively validate
// incoming messages and relay them to the opponent.
const actions = {
    newGame(spark, {
        id, player: { name = 'Anonymous', country = 'Unknown' } = {}
    }) {
        const player = { name, country };
        players[id] = player;
        if (pool.length) {
            const oppId = pool.shift(),
               opponent = players[oppId],
             letterSets = getLetterSets();
            spark.write({ opponent, letterSets });
            sparks[oppId].write({ opponent: player, letterSets });
            players[id].oppId = oppId;
            opponent.oppId = id;
        } else {
            pool.push(id);
        }
    },

    endGame(spark, { id }) {
        const oppId = players[id].oppId;
        if (oppId) {
            sparks[oppId].end({ action: 'endGame' });
            players[oppId].oppId = null;
            //delete sparks[oppId];
            //delete players[oppId];
        }
        players[id].oppId = null;
        spark.end();
        //delete sparks[id];
        //delete players[id];
    },

    nextRound(spark, { id }) {
        const oppId = players[id].oppId;
        oppId && sparks[oppId].write({
            action: 'nextRound', id
        });
    },

    playWord(spark, {
        id, selected = '',
        score = { word: -1, length: 0, time: 0, total: -1 }
    }) {
        const oppId = players[id].oppId;
        oppId && sparks[oppId].write({
            action: 'playWord', id, selected, score
        });
    },

    updateName(spark, { id, name = 'Anonymous' }) {
        name = name.trim();
        const oppId = players[id].oppId;
        oppId && sparks[oppId].write({
            action: 'updateName', id, name
        });
    }
};


primus.on('connection', spark => {
    //console.log(spark.id + ': connection');
    let id;

    spark.on('data', msg => {
        //console.log(spark.id + ': ' + JSON.stringify(msg));
        id || (id = msg.id);
        if (id !== msg.id) {
            //console.log(spark.id + ': client id changed');
            actions.endGame(spark, { id });
        } else {
            sparks[id] = spark;
            actions[msg.action](spark, msg);
        }
    });

    spark.on('end', () => {
        //console.log(spark.id + ': end connection');
        delete sparks[id];
        removeFromPool(id);
        setTimeout(() => {
            if (!sparks[id] && players[id]) {
                const oppId = players[id].oppId;
                delete players[id];
                if (oppId) {
                    sparks[oppId] && sparks[oppId].end({ action: 'endGame' });
                    players[oppId] && (players[oppId].oppId = null);
                }
            }
        }, 20000);
    });
});

primus.on('invalid', (err, args) => {
    //console.log('Invalid ' + err.event);
});

primus.validate('data', (msg, next) => {
    const isValid = typeof msg === 'object' && msg.id && typeof actions[msg.action] === 'function';
    return next(isValid);
});
