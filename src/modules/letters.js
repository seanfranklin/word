import _random from 'lodash/random';

const letters = 'EARIOTNSLCUDPMHGBFYWKVXZJQ ',
      weights = '975555554333333222221111110',
       points = '111111111111111222223333330';

function getTotalWeight() {
    return weights.split('').reduce((t, w) => t + parseInt(w), 0);
}

function getRndLetter() {
    const rnd = Math.random() * getTotalWeight();
    for (let i = 0, sum = 0; i < letters.length; i++) {
        sum += parseInt(weights.charAt(i)) ;
        if (rnd <= sum) return letters.charAt(i);
    }
}

function getPoints(letter) {
    const p = parseInt(points.charAt(letters.indexOf(letter)));
    return isNaN(p) ? 0 : p;
}

function getLetterSet(numOrStr = 7) {
    let cons = 0,
      vowels = 0,
         set = [],
         num = typeof numOrStr === 'number' ? numOrStr : typeof numOrStr === 'string' ? numOrStr.length : 7,
         str = typeof numOrStr === 'string' ? numOrStr : '';

    for (let i = 0; set.length < num; i++) {
        const letter = str.charAt(i) || getRndLetter(),
             isVowel = !!~'AEIOU'.indexOf(letter),
               bgPos = `${_random(1, 90)}% ${_random(1, 90)}%`;

        if (str || (!isVowel && cons++ < 5) || (isVowel && vowels++ < 5)) {
            const points = getPoints(letter);
            set.push({ letter, points, bgPos, selected: false });
        }
    }
    return set;
}

function testFrequency(num = 900) {
    const freqs = { E: 0 };
    while (freqs.E < num) {
        const letter = getRndLetter();
        freqs[letter] ? freqs[letter]++ : (freqs[letter] = 1);
    }
    let str = '';
    for (let i = 0; i < letters.length; i++) {
        const letter = letters.charAt(i);
        str += (freqs[letter] / (num / 9)).toFixed(0);
    }
}

export { getLetterSet, getPoints, testFrequency };
