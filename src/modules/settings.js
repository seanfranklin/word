export default {
    difficulty: {
        easy:    '120 second base timer. Automatic word selection. No unlocks.',
        normal:  '60 second base timer.',
        hard:    '30 second base timer. No points for 3-letter words.'
    },

    tileset: {
        plastic:  ' ',
        maple:    ' ',
        mahogany: ' ',
        metal:    ' ',
        marble:   ' ',
        jade:     ' ',
        pearl:    ' ',
        ruby:     ' ',
        random:   ' '
    },

    dictionary: {
        SOWPODS: 'Commonly used for word games. <br>190,000 English words of 3-10 letters.',
        OWL:     'Used by tournament Scrabblers in North America. <br>140,000 words of 3-10 letters.'
    }
};
