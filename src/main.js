/* globals Howl, app, bus */

require('./styles/fonts.css');
require('./styles/normalize.css');
require('./styles/print.css');
require('./styles/animate.css');
require('./styles/flags.css');

require('howler');

import Vue from 'vue';
import loadScript from './modules/loadScript';
import store from './modules/store';
import App from './App.vue';

navigator.onLine && loadScript('/primus.js');

window.primus = {};
window.bus = new Vue();

window.app = new Vue({
    el: '#app', store,
    render: h => h(App)
});

window.sounds = {
    click:   new Howl({ src: ['sounds/click.webm',   'sounds/click.ogg',   'sounds/click.wav'  ] }),
    plastic: new Howl({ src: ['sounds/plastic.webm', 'sounds/plastic.ogg', 'sounds/plastic.wav'] }),
    wood:    new Howl({ src: ['sounds/wood.webm',    'sounds/wood.ogg',    'sounds/wood.wav'   ] }),
    metal:   new Howl({ src: ['sounds/metal.webm',   'sounds/metal.ogg',   'sounds/metal.wav'  ] }),
    stone:   new Howl({ src: ['sounds/stone.webm',   'sounds/stone.ogg',   'sounds/stone.wav'  ] }),
    whoosh:  new Howl({ src: ['sounds/whoosh.webm',  'sounds/whoosh.ogg',  'sounds/whoosh.wav' ] })
};

window.addEventListener('resize', () => {
    app.$el.style.height = window.innerHeight + 'px';
    bus.$emit('window-resize');
});

window.addEventListener('online',  () => { bus.$emit('online');  });
window.addEventListener('offline', () => { bus.$emit('offline'); });

if (typeof cordova === 'undefined' && 'serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(reg => {
        //console.info('Service worker registered with scope: ', reg.scope);
    }).catch(error => {
        console.error('Service worker not registered: ', error);
    });
}
